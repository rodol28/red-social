const router = require('../routes/index.routes');

const express = require('express');
const path = require('path');

const exphbs = require('express-handlebars');
const morgan = require('morgan');
const multer = require('multer');
const errorhandler = require('errorhandler');

require('dotenv').config();

module.exports = app => {
    // settings
    app.set('port', process.env.PORT || 3000);
    app.set('views', path.join(__dirname, '../views'));
    app.engine('.hbs', exphbs({
        defaultLayout: 'main',
        layoutsDir: path.join(app.get('views'), 'layouts'),
        partialsDir: path.join(app.get('views'), 'partials'),
        extname: '.hbs',
        helpers: require('./helpers')
    }));
    app.set('view engine', '.hbs');

    // middlewares
    app.use(morgan('dev'));
    app.use(multer({
        dest: path.join(__dirname, '../public/uploads/temp')
    }).single('image'));
    app.use(express.urlencoded({
        extended: false
    }));
    app.use(express.json());


    // static files
    app.use('/public', express.static(path.join(__dirname, '../public')));

    // errorhandler
    if ('development' === process.env.NODE_ENV) {
        app.use(errorhandler());
    };

    // routes
    app.use(router);

    return app;
};