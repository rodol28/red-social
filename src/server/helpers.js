const moment = require('moment');

const herpers = {};

herpers.timeago = timestamp => {
    return moment(timestamp).startOf('minute').fromNow();
};

module.exports = herpers;