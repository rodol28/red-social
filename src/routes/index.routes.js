const router = require('express').Router();
const home = require('../controllers/index.ctrl');
const image_ctrl = require('../controllers/image.ctrl');

router.get('/', home.index);
router.get('/images/:image_id', image_ctrl.index);
router.post('/images', image_ctrl.create);
router.post('/images/:image_id/like', image_ctrl.like);
router.post('/images/:image_id/comment', image_ctrl.comment);
router.delete('/images/:image_id', image_ctrl.remove);

module.exports = router;