const { Schema, model } = require('mongoose');
const { ObjectId } = Schema;

const CommentSchema = new Schema({
    image_id: { type: ObjectId },
    name: { type: String },
    email: { type: String },
    comment: { type: String },
    timestamp: { type: Date, default: Date.now },
    gravatar: { type: String }
});

module.exports = model('Comment', CommentSchema);