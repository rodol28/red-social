const control = {};
const { Image } = require('../models/index');
const sidebar = require('../helpers/sidebar');

control.index = async (req, res) => {
    const images = await Image.find().sort({timestamp: -1}).lean();
    let viewModel = {images: []};
    viewModel.images = images;
    viewModel = await sidebar(viewModel)
    console.log(viewModel.sidebar.comments);
    res.render('index', viewModel);
};

module.exports = control;