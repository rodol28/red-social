const path = require('path');
const { randomNumber } = require('../helpers/libs');
const fs = require('fs-extra');
const md5 = require('md5');
const { Image, Comment } = require('../models'); //sirve como indice de todos los modelos, reemplaza la linea de abajo
// const image = require('../models/image');
const sidebar = require('../helpers/sidebar');
const control = {};

control.index = async (req, res) => {
    let viewModel = {image: {}, comments: {}};
    const img = await Image.findOne({uniqueId: req.params.image_id});
    if(img) {
        img.views = img.views + 1;
        await img.save();
    
        const image = await Image.findOne({uniqueId: req.params.image_id}).lean();
        viewModel.image = image;
        const comments = await Comment.find({image_id: image._id}).lean();
        viewModel.comments = comments;
        viewModel = await sidebar(viewModel);
        res.render('image', viewModel);
    } else {
        res.redirect('/');
    }

};

control.create = (req, res) => {

    const saveImage = async () => {

        const imgURL = randomNumber();
        const images = await Image.find({
            filename: imgURL
        });
        if (images.length > 0) {
            saveImage();
        } else {
            const imageTempPath = req.file.path;
            const ext = path.extname(req.file.originalname).toLowerCase();
            const targetPath = path.resolve(`src/public/uploads/${imgURL}${ext}`);

            if (ext === '.png' || ext === '.jpg' || ext === '.jpeg' || ext === '.gif') {
                try {
                    await fs.move(imageTempPath, targetPath);
                    const newImg = new Image({
                        title: req.body.title,
                        description: req.body.description,
                        filename: imgURL + ext,
                        uniqueId: imgURL
                    });
                    const imageSave = await newImg.save();
                    res.redirect('/images/' + imgURL);
                } catch (err) {
                    console.error(err);
                }
            } else {
                await fs.remove(imageTempPath);
                res.status(500).json({
                    error: 'Only images are allowed'
                });
            }
        }
    };

    saveImage();

};

control.like = async (req, res) => {
    const image = await Image.findOne({filename: req.params.image_id});
    if(image) {
        image.likes = image.likes + 1;
        await image.save();
        res.json({likes: image.likes});
    } else {
        res.status(500).json({error: 'Internal error.'});
    }
};

control.comment = async (req, res) => {
    const image = await Image.findOne({
        uniqueId: req.params.image_id
    });
    if (image) {
        const newComment = new Comment(req.body);
        newComment.gravatar = md5(newComment.email);
        newComment.image_id = image._id;
        await newComment.save();
        res.redirect('/images/' + image.uniqueId);
    } else {
        res.redirect('/');
    }

};

control.remove = async (req, res) => {
    const image = await Image.findOne({filename: {$regex: req.params.image_id}});
    if(image) {
        await fs.remove(path.resolve('./src/public/uploads/' + image.filename));
        await Comment.deleteOne({image_id: image._id});
        await image.remove();
        res.json(true)
    }
    res.send('Remove a image');
};

module.exports = control;